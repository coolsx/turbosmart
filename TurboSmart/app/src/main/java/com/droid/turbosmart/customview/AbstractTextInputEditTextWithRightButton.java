package com.droid.turbosmart.customview;

import android.content.Context;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.design.widget.TextInputEditText;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

/**
 * Created by quanganh.nguyen on 3/2/2016.
 */
public abstract class AbstractTextInputEditTextWithRightButton extends TextInputEditText {
    protected int idDrawableRightFocus = 0, idDrawableRightNotFocus = 0;
    protected Context context;
    protected boolean hideRightDrawable = false;
    protected View myParent;

    protected int idDrawableLeft = 0;

    public AbstractTextInputEditTextWithRightButton(Context context) {
        super(context);
        this.init(context);
    }

    public AbstractTextInputEditTextWithRightButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.init(context);
    }

    public AbstractTextInputEditTextWithRightButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.init(context);
    }

    protected final int getIdDrawableLeft() {
        if (idDrawableLeft < 1) {
            idDrawableLeft = android.R.color.transparent;
        }
        return idDrawableLeft;
    }

    protected void init(Context context) {
        this.context = context;
        this.idDrawableRightFocus = getDrawableRightOnFocus();
        this.idDrawableRightNotFocus = getDrawableRightNotFocus();
        hideDrawableRight(true);
    }

    protected void hideDrawableRight(boolean b) {
        this.hideRightDrawable = b;
        this.setCompoundDrawablesWithIntrinsicBounds(getIdDrawableLeft(), 0, 0, 0);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_UP) {
            Drawable right = this.getCompoundDrawables()[2];
            if (right != null && event.getRawX() >= this.getRight() - right.getBounds().width()) {
                this.onRightClick();
            }
        }
        return super.onTouchEvent(event);
    }

    @Override
    protected void onFocusChanged(boolean focused, int direction, Rect previouslyFocusedRect) {
        super.onFocusChanged(focused, direction, previouslyFocusedRect);
        this.redrawDrawableRight(focused);
    }

    public void setMyParent(View parent) {
        this.myParent = parent;
    }

    protected void redrawDrawableRight(boolean focused) {
        if (!hideRightDrawable) {
            if (focused && idDrawableRightFocus > 0) {
                this.setCompoundDrawablesWithIntrinsicBounds(getIdDrawableLeft(), 0, idDrawableRightFocus, 0);
            } else if (!focused && idDrawableRightNotFocus > 0) {
                this.setCompoundDrawablesWithIntrinsicBounds(getIdDrawableLeft(), 0, idDrawableRightNotFocus, 0);
            }
        }
    }

    public abstract void onRightClick();

    public abstract int getDrawableRightOnFocus();

    public abstract int getDrawableRightNotFocus();

}
