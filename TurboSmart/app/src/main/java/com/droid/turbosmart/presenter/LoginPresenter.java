package com.droid.turbosmart.presenter;

import com.droid.turbosmart.apis.LoginRequest;
import com.droid.turbosmart.interfaces.BasePresenter;
import com.droid.turbosmart.interfaces.BaseView;
import com.droid.turbosmart.interfaces.LoginView;
import com.droid.turbosmart.models.UserInfo;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseUser;

/**
 * Created by KIMSON on 10/5/2016.
 */

public class LoginPresenter implements BasePresenter {
    private LoginView loginView;
    private LoginRequest loginRequest;

    public LoginPresenter(LoginView loginView) {
        this.loginView = loginView;
    }

    @Override
    public void onCreate(BaseView view) {
    }

    @Override
    public void onDestroy() {
    }

    public void sendLogin (String email, String password) {
        if(loginRequest == null) {
            loginRequest = new LoginRequest();
        }

        loginView.showLoadingIndicator ();

        loginRequest.requestAPI(email, password, new LogInCallback() {
            @Override
            public void done(ParseUser user, ParseException e) {
                loginView.dismissLoadingIndicator();
                if (e != null) {
                    loginView.showError(e.getMessage());
                    return;
                }

                loginView.successLogin((UserInfo) user);
            }
        });
    }
}
