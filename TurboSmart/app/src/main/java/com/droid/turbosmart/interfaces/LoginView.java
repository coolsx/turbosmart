package com.droid.turbosmart.interfaces;

import com.droid.turbosmart.models.UserInfo;

/**
 * Created by KIMSON on 10/5/2016.
 */

public interface LoginView {
    /**
     * Show error message
     *
     * @param errorMessage
     */
    void showError(String errorMessage);

    /**
     * Call when successfully login into app.
     * @param userInfo: Return user info to used in app
     */
    void successLogin (UserInfo userInfo);

    /**
     * Show progress dialog when doing heavy tasks (E.g: get data from backend)
     */
    void showLoadingIndicator();

    /**
     * Dismiss progress dialog. This method is usually called when long-run task finishes
     */
    void dismissLoadingIndicator();

    boolean validate();
}
