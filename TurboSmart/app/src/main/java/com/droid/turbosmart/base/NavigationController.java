package com.droid.turbosmart.base;

/**
 * Created by johnnhat on 10/4/16.
 */

public interface NavigationController {

    void openLoginPage();

    void openMainPage();

    void openSettingPage();

    void openResetPasswordPage();

    void openRegistrationPage();

    void openDetailProductPage();

    void openScannerPage();


}
