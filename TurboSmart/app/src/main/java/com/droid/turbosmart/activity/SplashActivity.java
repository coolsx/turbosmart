package com.droid.turbosmart.activity;

import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.droid.turbosmart.R;
import com.droid.turbosmart.base.BaseActivity;
import com.droid.turbosmart.base.ParseBaseActivity;
import com.parse.ParseUser;

public class SplashActivity extends ParseBaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (isUserLoggedIn()) {
                    openMainPage();
                } else {
                    openLoginPage();
                }
            }
        }, 1000);

    }

    @Override
    protected boolean allowAutoCheckUserStatus() {
        return false;
    }
}
