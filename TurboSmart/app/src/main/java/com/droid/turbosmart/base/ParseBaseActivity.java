package com.droid.turbosmart.base;

import com.parse.ParseUser;

/**
 * Created by johnnhat on 10/5/16.
 */

public class ParseBaseActivity extends BaseActivity {

    protected boolean allowAutoCheckUserStatus() {
        return true;
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (allowAutoCheckUserStatus()) {
            checkUser();
        }
    }

    private void checkUser() {
        if (!isUserLoggedIn()) {
            this.openLoginPage();
            this.finish();
        }
    }

    protected final boolean isUserLoggedIn() {
        if (ParseUser.getCurrentUser() == null) {
            return false;
        }
        return true;
    }

    @Override
    public int getLayoutResource() {
        return 0;
    }
}
