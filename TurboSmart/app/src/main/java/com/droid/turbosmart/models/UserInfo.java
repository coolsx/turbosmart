package com.droid.turbosmart.models;

import com.parse.ParseException;
import com.parse.ParseUser;

/**
 * Created by SK on 10/4/2016.
 */

public class UserInfo extends ParseUser {
    private String username;
    private String email;

    public UserInfo(){}

    @Override
    public String getEmail() {
        this.email = (String) getCurrentUser().get("email");
        return email;
    }

    public String getUserName() {
        try {
            this.username = (String) this.fetchIfNeeded().get("username");
        } catch (ParseException e) {
            this.username = "";
            e.printStackTrace();
        }
        return username;
    }
}
