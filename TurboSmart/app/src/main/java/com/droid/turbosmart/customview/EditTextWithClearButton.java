package com.droid.turbosmart.customview;

import android.content.Context;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.text.TextWatcher;
import android.util.AttributeSet;

import com.droid.turbosmart.R;

/**
 * Created by quanganh.nguyen on 3/2/2016.
 */
public class EditTextWithClearButton extends AbstractTextInputEditTextWithRightButton {

    private TextWatcher listener;

    public EditTextWithClearButton(Context context) {
        super(context);
        this.init();
    }

    public EditTextWithClearButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.init();
    }

    public EditTextWithClearButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.init();
    }

    public void setDrawableLeft(int resourceLeft) {
        this.idDrawableLeft = resourceLeft;
        super.init(getContext());
    }

    private void init() {
        this.hideDrawableRight(true);
    }

    @Override
    protected void onTextChanged(CharSequence text, int start, int lengthBefore, int lengthAfter) {
        super.onTextChanged(text, start, lengthBefore, lengthAfter);
        if (listener != null) {
            listener.onTextChanged(text, start, lengthBefore, lengthAfter);
        }
        if (text.length() > 0) {
            this.hideDrawableRight(false);
            this.redrawDrawableRight(isFocused());
        } else {
            this.hideDrawableRight(true);
        }
    }


    @Override
    protected void onFocusChanged(boolean focused, int direction, Rect previouslyFocusedRect) {
        super.onFocusChanged(focused, direction, previouslyFocusedRect);
        if (myParent != null && focused) {
            this.myParent.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.ic_clear_text));
        } else if (myParent != null && !focused) {
            this.myParent.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.ic_clear_text));
        }
    }

    @Override
    public void onRightClick() {
        this.setText("");
    }

    @Override
    public int getDrawableRightOnFocus() {
        return R.drawable.ic_clear_text;
    }

    @Override
    public int getDrawableRightNotFocus() {
        return R.drawable.ic_clear_text;
    }

    @Override
    public void addTextChangedListener(TextWatcher watcher) {
        this.listener = watcher;
    }
}
