package com.droid.turbosmart.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import com.droid.turbosmart.activity.HomeActivity;

/**
 * Created by SK on 10/4/2016.
 */

public class BaseFragment extends Fragment {
    protected HomeActivity homeActivity;
    int fragId;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        homeActivity = (HomeActivity) this.getActivity();
    }
}
