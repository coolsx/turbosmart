package com.droid.turbosmart.base;

import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseUser;

/**
 * Created by johnnhat on 10/4/16.
 */

public class ParseBaseFragment extends BaseFragment implements LogInCallback {

    @Override
    public void done(ParseUser user, ParseException e) {

    }

    protected void onUserLoggedOut(){
    }
}
