package com.droid.turbosmart.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.droid.turbosmart.R;
import com.droid.turbosmart.base.BaseActivity;
import com.droid.turbosmart.customview.EditTextWithClearButton;
import com.droid.turbosmart.interfaces.BaseView;
import com.droid.turbosmart.interfaces.LoginView;
import com.droid.turbosmart.models.UserInfo;
import com.droid.turbosmart.presenter.LoginPresenter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by SK on 10/4/2016.
 */

public class LoginActivity extends BaseActivity implements LoginView {

    @BindView(R.id.edtEmail)
    EditTextWithClearButton edEmail;
    @BindView(R.id.edtPassword)
    EditTextWithClearButton edPass;
    @BindView(R.id.btnLogin)
    Button btnLogin;

    @BindView(R.id.textInputLayoutEmail)
    TextInputLayout textInputLayoutEmail;

    @BindView(R.id.textInputLayoutPassword)
    TextInputLayout textInputLayoutPassword;

    private LoginPresenter loginPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loginPresenter = new LoginPresenter(this);
        initView();
    }

    private void initView() {
        edEmail.setDrawableLeft(R.drawable.ic_email);
        edPass.setDrawableLeft(R.drawable.ic_password);
    }

    @Override
    public int getLayoutResource() {
        return R.layout.activity_login;
    }

    @OnClick(R.id.btnLogin)
    void doLogIn() {
        //TODO: Check validate email and password here.
        if (validate())
            loginPresenter.sendLogin(edEmail.getText().toString().trim(), edPass.getText().toString().trim());
    }

    @Override
    public void showError(String errorMessage) {
        Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void successLogin(UserInfo userInfo) {
        //TODO: Save userInfo to preference here

        // Start new Activity
        Intent intent = new Intent(this, HomeActivity.class);
        startActivity(intent);
    }

    @Override
    public void showLoadingIndicator() {
        //TODO: Show progressbar here
    }

    @Override
    public void dismissLoadingIndicator() {
        //TODO: Hide progressbar here
    }

    @Override
    public boolean validate() {
        if (TextUtils.isEmpty(edEmail.getText().toString())) {
            textInputLayoutEmail.setError(getString(R.string.error_missing_email));
            return false;
        }
        textInputLayoutEmail.setError(null);
        if (TextUtils.isEmpty(edPass.getText().toString())) {
            textInputLayoutPassword.setError(getString(R.string.error_missing_password));
            return false;
        }
        if (edPass.getText().toString().trim().length() < 5) {
            textInputLayoutPassword.setError(getString(R.string.error_password_length));
            return false;
        }
        textInputLayoutPassword.setError(null);
        return true;
    }
}
