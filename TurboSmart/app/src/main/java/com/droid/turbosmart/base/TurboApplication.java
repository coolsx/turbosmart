package com.droid.turbosmart.base;

import android.app.Application;

import com.parse.Parse;
import com.parse.ParseFacebookUtils;

/**
 * Created by SK on 10/4/2016.
 */

public class TurboApplication extends Application {

    private static final String APPLICATION_ID = "";
    private static final String CLIENT_KEY = "";

    @Override
    public void onCreate() {
        super.onCreate();

        initParse();
    }


    /**
     * Initialize parse and declare all sub class parse in app.
     */
    private void initParse() {
        // TODO: Register sub class parse and initialize parse
        Parse.initialize(new Parse.Configuration.Builder(getApplicationContext())
                .applicationId(APPLICATION_ID)
                .clientKey(CLIENT_KEY)
                .build());
        ParseFacebookUtils.initialize(getApplicationContext());
    }
}
