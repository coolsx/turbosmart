package com.droid.turbosmart.apis;

import com.droid.turbosmart.models.UserInfo;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseUser;

/**
 * Created by SK on 10/4/2016.
 */

public class LoginRequest {
    public void requestAPI(String email, String password, LogInCallback logInCallback) {
        UserInfo.logInInBackground(email, password, logInCallback);
    }
}
