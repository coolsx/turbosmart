package com.droid.turbosmart.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.droid.turbosmart.R;
import com.droid.turbosmart.base.BaseActivity;
import com.droid.turbosmart.base.NavigationController;

/**
 * Created by SK on 10/4/2016.
 */

public class HomeActivity extends BaseActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public int getLayoutResource() {
        return R.layout.activity_splash;
    }
}
