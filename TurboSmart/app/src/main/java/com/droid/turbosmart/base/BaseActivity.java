package com.droid.turbosmart.base;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.transition.Fade;
import android.transition.TransitionManager;

import com.droid.turbosmart.activity.HomeActivity;
import com.droid.turbosmart.activity.LoginActivity;

import butterknife.ButterKnife;

/**
 * Created by SK on 10/4/2016.
 */

public abstract class BaseActivity extends AppCompatActivity implements NavigationController {

    private void startIntentWithDefaultTransition(Intent intent) {
        if (intent == null) {
            throw new NullPointerException("intent can not be null");
        }
        startActivity(intent);
        overridePendingTransition(0, 0);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getLayoutResource() > 0) {
            setContentView(getLayoutResource());
        }
        ButterKnife.bind(this);
    }

    public abstract int getLayoutResource();

    @Override
    public void openLoginPage() {
        Intent loginIntent = new Intent(this, LoginActivity.class);
        startIntentWithDefaultTransition(loginIntent);
        this.finish();
    }


    @Override
    public void openMainPage() {
        Intent homeIntent = new Intent(this, HomeActivity.class);
        startIntentWithDefaultTransition(homeIntent);
        this.finish();
    }

    @Override
    public void openSettingPage() {

    }

    @Override
    public void openResetPasswordPage() {

    }

    @Override
    public void openRegistrationPage() {

    }

    @Override
    public void openDetailProductPage() {

    }

    @Override
    public void openScannerPage() {

    }
}
